const Faker = require('faker')

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
}

module.exports = () => {
    const data = {
        users: [
            {
                "id": 1,
                "name": "User",
                "email": "user@gmail.com",
                "password": "$2a$10$bkQYGQb2UOCGpGV6ym5kEuedLuJ4BeMhOKC2CIozq92FMeB0bJzlm"
            }
        ],
        chats: [
            {
                "id": 1,
                "name": "Рабочий чат",
                "icon": "dashboard",
                "members": []
            },
            {
                "id": 2,
                "name": "Общение",
                "icon": "contacts",
                "members": []
            },
        ],
        profiles: [{
            id: 1,
            name: 'User',
            status: 'online'
        }],
        messages: []
    }

    const status = ['online', 'away', 'offline', 'dnd']

    for (let i = 2; i <= 50; i++) {
        data.profiles.push({
            id: i,
            name: `${Faker.name.firstName()} ${Faker.name.lastName()}`,
            status: status[Math.floor(Math.random() * status.length)]
        })
    }

    for (let i = 1; i < 25; i++) {
        let randomLike = Faker.random.arrayElement([0, 0, 0, 0, 0, 0, 1, 3, 4, 0, 2, 5])
        let likes = new Set()
        if (randomLike) {
            for (let i = 0; i < randomLike; i++) {
                likes.add(data.profiles[Math.floor(Math.random() * data.profiles.length)].id)
            }
        }
        data.messages.push({
            id: i,
            userId: data.profiles[Math.floor(Math.random() * data.profiles.length)].id,
            chatId: getRandomIntInclusive(1, 2),
            likedBy: Array.from(likes),
            message: Faker.lorem.text(),
            timestamp: Faker.date.between('2020-08-21 11:16', '2020-08-21 23:16')
        })
    }

    data.chats.forEach(item => {
        for (let i = 1; i < getRandomIntInclusive(10, 20); i++) {
            item.members.push(getRandomIntInclusive(1, data.profiles.length))
        }
    })


    return data
}