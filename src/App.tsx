import React, {useEffect, useState} from 'react'
import {createMuiTheme, CssBaseline} from "@material-ui/core";
import {AppDispatch, RootState} from "./store/store";
import {connect} from "react-redux";
import {animated, useTransition} from 'react-spring'
import FirstLoadPlaceholder from "./components/FirstLoadPlaceholder";
import LoginPage from "./pages/LoginPage";
import {checkAuth} from './store/slices/SessionSlice';
import ProgressBar from "./components/ProgressBar";
import {ThemeProvider} from "@material-ui/styles";
import {Palette} from "@material-ui/core/styles/createPalette";
import {COLORS} from "./styles/colors";
import ChatPage from "./pages/ChatPage";

type AppPropsType = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const App: React.FC<AppPropsType> = props => {

    const {checkAuth, token} = props

    const items = {
        firstLoadPlaceHolder: {id: 0, value: <FirstLoadPlaceholder/>},
        login: {id: 1, value: <LoginPage/>},
        mainApp: {id: 2, value: <ChatPage/>},
    }

    const [index, setIndex] = useState<keyof typeof items>('firstLoadPlaceHolder')

    useEffect(() => {
        if (token === null) checkAuth()
        else if (token === '') setIndex('login')
        else setIndex('mainApp')
    }, [token])

    const authTransitions = useTransition(items[index], item => item.id, {
        from: {position: 'absolute', opacity: 0},
        enter: {opacity: 1},
        leave: {opacity: 0}
    })

    const theme = {
        palette: {
            type: 'dark',
            primary: {main: COLORS.main},
            secondary: {main: COLORS.dark},
            background: {
                paper: COLORS.surface.main,
                default: COLORS.surface.darkest,
            }
        } as Palette,
        breakpoints: {
            values: {
                xs: 599,
                sm: 600,
                md: 900,
                lg: 1200,
                xl: 1800
            }
        },
    }

    return (
        <ThemeProvider theme={createMuiTheme(theme)}>
            <CssBaseline/>
            <ProgressBar/>
            {authTransitions.map(({item, props, key}) =>
                <animated.div key={key} style={{width: '100%', height: '100%', ...props}}>
                    {item.value}
                </animated.div>
            )}
        </ThemeProvider>
    )
}


const mapStateToProps = (state: RootState) => {
    return {
        token: state.Session.token
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        checkAuth: () => dispatch(checkAuth())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
