export const COLORS = {
    main: '#66bb6a',
    dark: '#c8e6c9',
    surface: {
        light: '#40444b',
        main: '#36393f',
        dark: '#2f3136',
        darkest: '#26282d'

    },
    status: {
        online: '#4dff4d',
        offline: '#7d828a',
        away: '#FCB316',
        dnd: '#f95466'
    }
}