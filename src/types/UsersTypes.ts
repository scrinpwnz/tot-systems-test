export type UserItemType = {}

export type UserStatus = 'online' | 'offline' | 'away' | 'dnd'