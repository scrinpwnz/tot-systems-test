import {MessageItemType} from "./ChatTypes";

export type LoginResponseType = {
    accessToken: string
}

export type EditMessagePayloadType = Pick<MessageItemType, 'message'>

export type AddMessagePayloadType = Omit<MessageItemType, 'id' | 'timestamp'>

export type EditedMessageItemType = EditMessagePayloadType & Pick<MessageItemType, 'id'>
