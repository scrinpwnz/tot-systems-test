import React from "react";
import {UserStatus} from "./UsersTypes";

export type FindChatType = {
    (chats: Array<ChatItemWithDataType>, contact: ChatItemWithDataType): ChatItemWithDataType
}

export type FindMessageByChatIdAndMessageIdType = {
    (chats: Array<ChatItemWithDataType>, chatId: number, messageId: number): MessageItemTypeWithState
}

export type MessageItemType = {
    id: number
    userId: number
    chatId: number
    likedBy: Array<number>
    message: string
    timestamp: string
}

export type MessageItemTypeWithState = MessageItemType & MessageStateType

export type MessageStateType = {
    editing: StatusStateType & { editingMode: boolean }
}

export type StatusStateType = {
    success: boolean
    inProgress: boolean
}

export type ChatItemType = {
    id: number
    name: string
    icon: string
    members: Array<number>
}

export type ChatDataType = {
    messages: Array<MessageItemTypeWithState>
    membersData: Array<ProfileItemType>
    textArea: string
}

export type ChatItemWithDataType = ChatItemType & ChatDataType

export type ProfileItemType = {
    id: number
    name: string
    status: UserStatus
}


export type MembersList = Array<React.ReactNode>