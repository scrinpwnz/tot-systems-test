export const getDateObject = (date = null as null | string | number | Date) => {

    const addZero = (num: number) => {
        return (num < 10) ? '0' + num : num
    }

    let d: Date

    if (date === null) d = new Date()
    else if (date instanceof Date) d = date
    else d = new Date(date)

    return {
        hours: addZero(d.getHours()),
        minutes: addZero(d.getMinutes()),
        seconds: addZero(d.getSeconds()),
        day: addZero(d.getDate()),
        month: addZero(d.getMonth() + 1),
        year: d.getFullYear()
    }
}

export const getSQLDateTime = (date = null as null | string | number | Date) => {
    let t = getDateObject(date)
    return `${t.year}-${t.month}-${t.day} ${t.hours}:${t.minutes}:${t.seconds}`
}

export const getSQLDate = (date = null as null | string | number | Date) => {
    let t = getDateObject(date)
    return `${t.year}-${t.month}-${t.day}`
}

export const getFormattedDate = (date = null as null | string | number | Date) => {
    let t = getDateObject(date)
    return {
        time: `${t.hours}:${t.minutes}:${t.seconds}`,
        date: `${t.day}.${t.month}.${t.year.toString().slice(-2)}`,
        dateFullYear: `${t.day}.${t.month}.${t.year}`
    }
}

export const setTimeToZero = (date: Date) => {
    date.setHours(0)
    date.setMinutes(0)
    date.setSeconds(0)
    date.setMilliseconds(0)
    return date
}

export function logDuration<T extends (...args: any[]) => any>(func: T): (...funcArgs: Parameters<T>) => ReturnType<T> {
    const funcName = func.name

    return (...args: Parameters<T>): ReturnType<T> => {
        console.time(funcName)
        const results = func(...args)
        console.timeEnd(funcName)
        return results
    }
}

export const escapeSpecialCharacters = (string: string) => {
    return string.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")
}

type Lang = 'ru' | 'en'

export class KeySwitcher {

    protected map = {} as {
        [key: string]: string
    }

    protected languages = {
        'ru': [
            'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д',
            'ж', 'э', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю', '.', 'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ',
            'З', 'Х', 'Ъ', 'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э', 'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь',
            'Б', 'Ю', ','
        ],
        'en': [
            'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l',
            ';', '\'', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O',
            'P', '{', '}', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"', 'Z', 'X', 'C', 'V', 'B', 'N', 'M',
            '<', '>', '?'
        ]
    }

    public constructor(from: Lang, to: Lang) {
        this.createMap(from, to)
    }

    private createMap(from: Lang, to: Lang) {
        const languageFrom = this.languages[from]
        const languageTo = this.languages[to]
        languageFrom.forEach((key, index) => this.map[key] = languageTo[index])
    }

    public switchKey(key: string) {
        if (key in this.map) return this.map[key]
        return key
    }

    public switchString(string: string) {
        return string.replace(/[^]/g, match => {
            if (match in this.map) return this.map[match]
            return match
        })
    }

}

export const roundNumber = (number: number, fractionDigits = 2 as number) => {
    return +(number.toFixed(fractionDigits))
}

export const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))

export const stringToColor = (string: string) => {
    let hash = 0
    let i
    for (i = 0; i < string.length; i += 1) {
        hash = string.charCodeAt(i) + ((hash << 5) - hash)
    }
    let color = '#'
    for (i = 0; i < 3; i += 1) {
        const value = (hash >> (i * 8)) & 0xff
        color += `00${value.toString(16)}`.substr(-2)
    }
    return color
}