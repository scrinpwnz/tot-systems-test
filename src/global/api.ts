import axios from 'axios'
import store from "../store/store";
import {LoginPayloadType} from "../types/LoginTypes";
import {AddMessagePayloadType, EditMessagePayloadType, LoginResponseType} from "../types/APITypes";
import {ChatItemType, MessageItemType, ProfileItemType} from "../types/ChatTypes";

const instance = axios.create({
    baseURL: "http://localhost:3001/",
})

instance.interceptors.request.use(config => {
    config.headers.Authorization = `Bearer ${store.getState().Session.token}`
    return config;
})

export const API = {
    login: (payload: LoginPayloadType) => {
        return instance.post('login', {
            email: payload.email,
            password: payload.password
        }).then(response => response.data as LoginResponseType)
    },
    getChats: () => {
        return instance.get(`chats`).then(response => response.data as Array<ChatItemType>)
    },
    getProfiles: (ids: Array<number>) => {
        const query = ids.map(item => `id=${item}`).join('&') as string
        return instance.get(`profiles?${query}`).then(response => response.data as Array<ProfileItemType>)
    },
    getMessages: (chatId: number) => {
        return instance.get(`messages?chatId=${chatId}`).then(response => response.data as Array<MessageItemType>)
    },
    addMessage: (payload: Omit<AddMessagePayloadType, 'likedBy'>) => {
        return instance.post(`messages`, {
            ...payload,
            id: Math.floor(Math.random() * 10000 + 500),
            likedBy: [],
            timestamp: new Date()
        }).then(response => response.data as MessageItemType)
    },
    deleteMessage: (id: number) => {
        // return instance.delete(`messages/${id}/`).then(() => true)
        return true // json-server почему-то некорекктно работает, удаляет все
    },
    editMessage: (id: number, payload: EditMessagePayloadType) => {
        // return instance.put(`messages/${id}`, {
        //     message: payload.message,
        // }).then(response => response.data as EditedMessageItemType)
        return true // json-server почему-то некорекктно работает, удаляет одно, вместо редактирования
    },
};




