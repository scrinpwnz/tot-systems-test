import React, {useEffect} from 'react'
import Container from "@material-ui/core/Container";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {AppDispatch, RootState} from "../store/store";
import {connect} from "react-redux";
import Sidebar from "../components/Sidebar/Sidebar";
import Chat from "../components/Chat/Chat";
import {getChats} from "../store/slices/ChatSlice";
import {Route, Switch, useLocation} from "react-router";
import LoadingPlaceHolder from "../components/LoadingPlaceHolder";
import {Typography} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => ({
    appWrapper: {
        position: 'relative',
        top: theme.spacing(2),
        width: '100%',
        height: `calc(100% - ${theme.spacing(4)}px)`,
        padding: 0,
        background: '#2f3136',
        [theme.breakpoints.down('md')]: {
            top: 0,
            height: `100%`,
            margin: 0,
            maxWidth: 'none'
        },
    },
    root: {
        display: 'grid',
        height: '100%',
        gridTemplateColumns: 'fit-content(240px) minmax(600px, 1fr)',
        gridTemplateRows: '100%',
        overflow: 'auto',
    },
    chat: {
        overflowX: 'hidden',
    },
    container: {
        position: 'relative',
        height: '100%',
        padding: 0,
    },
    linearProgress: {
        position: 'absolute',
        width: '100%',
        display: 'none',
        zIndex: 10000
    },
    linearProgressActive: {
        display: 'block'
    },
    chatNotSelected: {
        display: 'grid',
        width: '100%',
        height: '100%',
        placeItems: 'center'
    }
}))

const ChatNotSelectedMessage = () => {

    const classes = useStyles()

    return (
        <div className={classes.chatNotSelected}>
            <Typography variant={'h4'}>
                Чат не выбран
                <span role={'img'}>😶</span>
            </Typography>
        </div>
    )
}

type ChatPageTypeProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const ChatPage: React.FC<ChatPageTypeProps> = props => {

    const classes = useStyles()

    const location = useLocation()

    const {getChats, chats} = props

    useEffect(() => {
        setTimeout(() => getChats(), 1000)
    }, [])

    return (
        <Container maxWidth="lg" className={classes.appWrapper}>
            <div className={classes.root}>
                <Sidebar/>
                {(chats.length > 0) ? <Switch location={location}>
                    <Route path={'/chat/:id'} component={Chat}/>
                    <Route exact path={'/'} component={ChatNotSelectedMessage}/>
                </Switch> : <LoadingPlaceHolder/>}
            </div>
        </Container>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        chats: state.Chat.chats
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        getChats: () => {
            dispatch(getChats())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatPage)

