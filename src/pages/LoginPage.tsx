import React, {ChangeEvent} from 'react'
import Container from "@material-ui/core/Container";
import {Checkbox, CircularProgress, FormControlLabel} from "@material-ui/core";
import {makeStyles, Theme} from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline/CssBaseline";
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button";
import {AppDispatch, RootState} from "../store/store";
import {connect} from "react-redux";
import CheckIcon from '@material-ui/icons/Check';
import {login, setEmail, setPassword, toggleRememberMe} from '../store/slices/LoginSlice';
import {LoginPayloadType} from "../types/LoginTypes";

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        height: '100vh',
        paddingTop: theme.spacing(1.5),
        paddingBottom: theme.spacing(1.5),
        display: 'flex',
        alignItems: 'center'
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}))


const LoginPage: React.FC<ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>> = props => {

    const classes = useStyles()

    const {
        email,
        password,
        setEmail,
        setPassword,
        toggleRememberMe,
        login,
        rememberMe,
        passwordHelperText,
        signInInProgress,
        isAuthenticated
    } = props

    const error = !!(passwordHelperText)

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        login({email, password, rememberMe})
    }

    const handleLogin = (e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setEmail(e.target.value)
    }

    const handlePassword = (e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setPassword(e.target.value)
    }

    const handleRememberMe = () => {
        toggleRememberMe()
    }

    return (
        <>
            <CssBaseline/>
            <Container maxWidth={'sm'}>
                <div className={classes.paper}>
                    <form className={classes.form} onSubmit={handleSubmit}>
                        <TextField
                            variant={'outlined'}
                            margin={'normal'}
                            required
                            fullWidth
                            label={'Логин'}
                            name={'email'}
                            autoComplete={'email'}
                            autoFocus
                            onChange={handleLogin}
                            value={email}
                        />
                        <TextField
                            error={error}
                            variant={'outlined'}
                            margin={'normal'}
                            required
                            fullWidth
                            label={'Пароль'}
                            name={'password'}
                            type={'password'}
                            autoComplete={'current-password'}
                            onChange={handlePassword}
                            value={password}
                            helperText={passwordHelperText}
                        />
                        <FormControlLabel
                            control={<Checkbox color={'primary'}
                                               checked={rememberMe}
                                               onChange={handleRememberMe}/>}
                            label={'Запомнить меня'}
                        />
                        <Button
                            type={'submit'}
                            fullWidth
                            variant={'text'}
                            color={'primary'}
                            disabled={signInInProgress || isAuthenticated}
                            className={classes.submit}
                        >
                            {signInInProgress
                                ? <CircularProgress size={24}/>
                                : (isAuthenticated)
                                    ? <CheckIcon/>
                                    : 'Вход'}
                        </Button>
                    </form>
                </div>
            </Container>
        </>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        email: state.Login.email,
        password: state.Login.password,
        rememberMe: state.Login.rememberMe,
        passwordHelperText: state.Login.passwordHelperText,
        signInInProgress: state.Login.signInInProgress,
        isAuthenticated: state.Login.isAuthenticated
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        setEmail: (login: string) => {
            dispatch(setEmail(login))
        },
        setPassword: (password: string) => {
            dispatch(setPassword(password))
        },
        toggleRememberMe: () => {
            dispatch(toggleRememberMe())
        },
        login: (payload: LoginPayloadType) => {
            dispatch(login(payload))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)

