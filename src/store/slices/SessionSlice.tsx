import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {AppThunk} from "../store";
import {sleep} from "../../global/helpers/helpers";

const initialState = {
    id: 1 as number | null,
    name: null as string | null,
    token: null as string | null,
    rememberMe: null as boolean | null
}

const Session = createSlice({
    name: 'Session',
    initialState,
    reducers: {
        setSession: (state, action: PayloadAction<{ token: string, rememberMe: boolean, name: string }>) => {
            localStorage.setItem('token', action.payload.token)
            localStorage.setItem('rememberMe', action.payload.rememberMe.toString())
            localStorage.setItem('name', action.payload.name)
            state.token = action.payload.token
            state.rememberMe = action.payload.rememberMe
            state.name = action.payload.name
        },
        setToken: (state, action: PayloadAction<string>) => {
            localStorage.setItem('token', action.payload)
            state.token = action.payload
        },
        clearSession: state => {
            localStorage.setItem('token', '')
            localStorage.removeItem('rememberMe')
            localStorage.removeItem('name')
            return initialState
        },
    },
})

export const checkAuth = (): AppThunk => async dispatch => {
    await sleep(0).then(() => {
        if (localStorage.getItem('rememberMe') === 'true') {
            const token = localStorage.getItem('token')
            const name = localStorage.getItem('name')!

            if (token !== null) dispatch(setSession({token, name, rememberMe: true}))
            else dispatch(setToken(''))
        } else dispatch(setToken(''))
    }) // таймаут для имитации задержки ответа от сервера
}

export const {
    setToken, setSession, clearSession
} = Session.actions

export default Session.reducer