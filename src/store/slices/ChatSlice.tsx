import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {AppThunk} from "../store";
import {inProgress} from "./AppSlice";
import {API} from "../../global/api";
import {
    ChatDataType,
    ChatItemType,
    ChatItemWithDataType,
    FindChatType,
    FindMessageByChatIdAndMessageIdType,
    MessageItemType,
    MessageItemTypeWithState,
    MessageStateType,
    ProfileItemType,
    StatusStateType
} from "../../types/ChatTypes";
import {AddMessagePayloadType, EditMessagePayloadType} from "../../types/APITypes";
import {sleep} from "../../global/helpers/helpers";

const messageStatusState = {
    success: false,
    inProgress: false
} as StatusStateType

const messageState = {
    editing: {...messageStatusState, editingMode: false},
} as MessageStateType

const chatData = {
    messages: [],
    membersData: [],
    textArea: '',
} as ChatDataType

const initialState = {
    chats: [] as Array<ChatItemWithDataType>,
}

const findChat: FindChatType = (chats, chat) => {
    return chats.find(item => item.id === chat.id)!
}

const findMessageByChatIdAndMessageId: FindMessageByChatIdAndMessageIdType = (chats, chatId, messageId) => {
    return chats.find(item => item.id === chatId)!.messages.find(item => item.id === messageId)!
}

const Chat = createSlice({
    name: 'Chat',
    initialState,
    reducers: {
        resetChatSlice: state => initialState,
        setChats: (state, action: PayloadAction<Array<ChatItemType>>) => {
            state.chats = action.payload.map(item => ({...item, ...chatData}))
        },
        setMessages: (state, action: PayloadAction<{ messages: Array<MessageItemType>, chat: ChatItemWithDataType }>) => {
            findChat(state.chats, action.payload.chat).messages
                = action.payload.messages.map(item => ({...item, ...messageState}))
        },
        setMembers: (state, action: PayloadAction<{ members: Array<ProfileItemType>, chat: ChatItemWithDataType }>) => {
            findChat(state.chats, action.payload.chat).membersData = action.payload.members
        },
        setTextAreaValue: (state, action: PayloadAction<{ value: string, chat: ChatItemWithDataType }>) => {
            findChat(state.chats, action.payload.chat).textArea = action.payload.value
        },
        addMessage: (state, action: PayloadAction<MessageItemType>) => {
            state.chats.find(item => item.id === action.payload.chatId)!
                .messages.push({...action.payload, likedBy: [], ...messageState})
        },
        likeMessage: (state, action: PayloadAction<MessageItemTypeWithState>) => {
            let likedBy = findMessageByChatIdAndMessageId(state.chats, action.payload.chatId, action.payload.id).likedBy
            if (!likedBy.includes(1)) likedBy.push(1)
            else findMessageByChatIdAndMessageId(state.chats, action.payload.chatId, action.payload.id).likedBy =
                likedBy.filter(item => item !== 1)
        },
        deleteMessage: (state, action: PayloadAction<MessageItemTypeWithState>) => {
            state.chats.find(item => item.id === action.payload.chatId)!.messages
                = state.chats.find(item => item.id === action.payload.chatId)!.messages
                .filter(item => item.id !== action.payload.id)
        },
        editMessage: (state, action: PayloadAction<{ message: MessageItemTypeWithState, payload: EditMessagePayloadType }>) => {
            findMessageByChatIdAndMessageId(state.chats, action.payload.message.chatId, action.payload.message.id)
                .message = action.payload.payload.message
        },
        setMessageEditingMode: (state, action: PayloadAction<{ message: MessageItemTypeWithState, payload: boolean }>) => {
            findMessageByChatIdAndMessageId(state.chats, action.payload.message.chatId, action.payload.message.id)
                .editing.editingMode = action.payload.payload
        },

    },
})

export const getChats = (): AppThunk => async dispatch => {
    try {
        dispatch(inProgress(true))
        const result = await API.getChats()
        if (result) {
            dispatch(setChats(result))
        }
    } catch (error) {
        console.log(error)
    } finally {
        dispatch(inProgress(false))
    }
}

export const getChatData = (chat: ChatItemWithDataType): AppThunk => async dispatch => {
    try {
        dispatch(inProgress(true))
        const messages = await API.getMessages(chat.id)
        const likedBy = [] as Array<number>
        messages.map(item => item.likedBy).forEach(item => likedBy.push(...item))
        const membersIds = Array.from(new Set([
            ...messages.map(item => item.userId),
            ...likedBy,
            ...chat.members,
            1
        ]))
        const members = await API.getProfiles(membersIds)
        sleep(500).then(() => {
            if (members) dispatch(setMembers({members, chat}))
            if (messages) dispatch(setMessages({messages, chat}))
        })
    } catch (error) {
        console.log(error)
    } finally {
        dispatch(inProgress(false))
    }
}

export const addMessageToDatabase = (payload: Omit<AddMessagePayloadType, 'likedBy'>): AppThunk => {
    return (async dispatch => {
        try {
            const result = await API.addMessage(payload)
            if (result) dispatch(addMessage(result))
        } catch (error) {
            console.log(error)
        } finally {
        }
    })
}

export const deleteMessageFromDatabase = (payload: MessageItemTypeWithState): AppThunk => {
    return (async dispatch => {
        try {
            const result = await API.deleteMessage(payload.id)
            if (result) dispatch(deleteMessage(payload))
        } catch (error) {
            console.log(error)
        } finally {
        }
    })
}

export const editMessageInDatabase = (message: MessageItemTypeWithState, payload: EditMessagePayloadType): AppThunk => {
    return (async dispatch => {
        try {
            const result = await API.editMessage(message.id, payload)
            if (result) {
                dispatch(editMessage({message, payload}))
                dispatch(setMessageEditingMode({message, payload: false}))
            }
        } catch (error) {
            console.log(error)
        } finally {
        }
    })
}

export const {
    resetChatSlice, setMessages, setChats, setMembers, addMessage, setTextAreaValue,
    likeMessage, deleteMessage, setMessageEditingMode, editMessage
} = Chat.actions

export default Chat.reducer