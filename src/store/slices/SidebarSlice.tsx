import {createSlice} from "@reduxjs/toolkit"

const initialState = {
    open: false as boolean,
}

const Sidebar = createSlice({
    name: 'Sidebar',
    initialState,
    reducers: {
        toggle: state => {
            state.open = !state.open
        },
    },
})

export const {toggle} = Sidebar.actions

export default Sidebar.reducer