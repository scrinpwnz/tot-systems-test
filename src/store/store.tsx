import {Action, combineReducers} from "redux";
import {configureStore, getDefaultMiddleware, ThunkAction} from "@reduxjs/toolkit";
import AppSlice from "./slices/AppSlice";
import SessionSlice from "./slices/SessionSlice";
import LoginSlice from "./slices/LoginSlice";
import SidebarSlice from "./slices/SidebarSlice";
import ChatSlice from "./slices/ChatSlice";


const rootReducer = combineReducers({
    App: AppSlice,
    Chat: ChatSlice,
    Session: SessionSlice,
    Login: LoginSlice,
    Sidebar: SidebarSlice,
})

const middleware = getDefaultMiddleware({
    immutableCheck: false,
    serializableCheck: false,
    thunk: true,
});

const store = configureStore({
    reducer: rootReducer,
    middleware,
    devTools: process.env.NODE_ENV !== 'production',
});

export default store
export type RootState = ReturnType<typeof rootReducer>
export type AppDispatch = typeof store.dispatch
export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>
