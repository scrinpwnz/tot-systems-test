import React from 'react'
import {makeStyles, Theme} from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }
}))

type LoadingPlaceHolderPropsType = {
    width?: number
    height?: number
}

const LoadingPlaceHolder: React.FC<LoadingPlaceHolderPropsType> = props => {

    const classes = useStyles()

    const {width, height} = props

    const style = {} as {
        width?: string
        height?: string
    }

    if (width) style.width = `${width}px`
    if (height) style.height = `${height}px`

    return (
        <div className={classes.container} style={style}>
            <CircularProgress size={48} variant="indeterminate"/>
        </div>
    )
}

export default LoadingPlaceHolder