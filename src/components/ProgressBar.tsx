import React from 'react'
import TopBarProgress from "react-topbar-progress-indicator"
import {connect} from 'react-redux'
import useTheme from "@material-ui/core/styles/useTheme";
import {RootState} from "../store/store";

type Props = ReturnType<typeof mapStateToProps>

const ProgressBar: React.FC<Props> = props => {

    const theme = useTheme()

    TopBarProgress.config({
        barColors: {
            "0": theme.palette.primary.light,
            "0.5": theme.palette.primary.main,
            "1.0": theme.palette.primary.dark,
        },
        shadowBlur: 1,
        barThickness: 6
    })

    const {inProgress} = props

    return (
        <>
            {(inProgress) ? <TopBarProgress/> : null}
        </>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        inProgress: state.App.inProgress
    }
}

export default connect(mapStateToProps)(ProgressBar)