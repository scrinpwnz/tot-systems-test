import React from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {COLORS} from "../../styles/colors";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(1),
        borderBottom: `${COLORS.surface.dark} 1px solid`
    },
    content: {
        display: 'flex',
        userSelect: 'none',
        color: theme.palette.text.disabled,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
}))

type ChatHeaderPropsType = {
    chatName: string
}

const ChatHeader: React.FC<ChatHeaderPropsType> = props => {

    const classes = useStyles()

    const {chatName} = props

    return (
        <div className={classes.root}>
            <div className={classes.content}>
                {chatName}
            </div>
        </div>
    )
}

export default ChatHeader