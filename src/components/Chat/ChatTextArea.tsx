import React, {ChangeEvent, useRef, useState} from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {COLORS} from "../../styles/colors";
import {Fab, TextField} from "@material-ui/core";
import {AppDispatch, RootState} from "../../store/store";
import {connect} from "react-redux";
import {addMessageToDatabase, setTextAreaValue} from "../../store/slices/ChatSlice";
import {ChatItemWithDataType} from "../../types/ChatTypes";
import {AddMessagePayloadType} from "../../types/APITypes";
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import {BaseEmoji, Picker} from 'emoji-mart'
import "emoji-mart/css/emoji-mart.css";
import clsx from 'clsx'

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
        paddingTop: 0,
    },
    container: {
        display: 'grid',
        gridTemplateColumns: '1fr fit-content(200px)',
        gridGap: theme.spacing(2),
        alignItems: 'center',
        position: 'relative'
    },
    surface: {
        background: COLORS.surface.light,
    },
    emojiPicker: {
        display: 'none',
        position: 'absolute',
        bottom: '40px',
        right: '20px'
    },
    emojiPickerOpen: {
        display: 'block'
    }
}))

type ChatTextAreaPropsType = {
    chat: ChatItemWithDataType
} & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const ChatTextArea: React.FC<ChatTextAreaPropsType> = props => {

    const classes = useStyles()

    const {setTextAreaValue, chat, addMessage} = props

    const messageRef = useRef<HTMLInputElement>(null)

    const handleChange = (e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setTextAreaValue(e.target.value, chat)
    }

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if ((event.key === 'Enter') && (messageRef.current)) {
            handleSubmit()
        }
    }

    const handleSubmit = () => {
        if (messageRef.current) {
            const payload = {
                userId: 1,
                chatId: chat.id,
                message: messageRef.current.value,
            }
            if (payload.message) {
                addMessage(payload)
                setTextAreaValue('', chat)
            }
        }
    }

    const [emojiOpen, setEmojiOpen] = useState<boolean>(false)

    const openEmojiPicker = () => {
        setEmojiOpen(true)
    }

    const handlePickerSelect = (emoji: BaseEmoji) => {
        if (messageRef.current) {
            setTextAreaValue(messageRef.current.value + emoji.native, chat)
            setEmojiOpen(false)
        }
    }


    return (
        <div className={classes.root}>
            <div className={classes.container}>
                <TextField
                    fullWidth
                    onKeyPress={handleKeyPress}
                    onChange={handleChange}
                    value={chat.textArea}
                    className={classes.surface}
                    inputRef={messageRef}
                    placeholder={`Написать в #${chat.name}`}
                    variant={'outlined'}
                />
                <Fab size={'small'} color={'primary'} onClick={openEmojiPicker}>
                    <InsertEmoticonIcon/>
                </Fab>
                <div className={clsx(classes.emojiPicker, emojiOpen && classes.emojiPickerOpen)}>
                    <Picker native
                            showSkinTones={false}
                            title={''}
                            onClick={handlePickerSelect}
                    />
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state: RootState) => {
    return {}
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        addMessage: (payload: Omit<AddMessagePayloadType, 'likedBy'>) => {
            dispatch(addMessageToDatabase(payload))
        },
        setTextAreaValue: (value: string, chat: ChatItemWithDataType) => {
            dispatch(setTextAreaValue({value, chat}))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatTextArea)
