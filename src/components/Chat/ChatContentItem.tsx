import React, {useRef} from "react";
import {makeStyles, Theme, useTheme} from "@material-ui/core/styles";
import {COLORS} from "../../styles/colors";
import {stringToColor} from "../../global/helpers/helpers";
import {Avatar, Badge, IconButton, Menu, MenuItem, TextField, Typography} from "@material-ui/core";
import {MessageItemTypeWithState, ProfileItemType} from "../../types/ChatTypes";
import StatusBadge from "../StatusBadge";
import clsx from 'clsx'
import {AppDispatch, RootState} from "../../store/store";
import {connect} from "react-redux";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Tooltip from "@material-ui/core/Tooltip";
import {
    deleteMessageFromDatabase,
    editMessageInDatabase,
    likeMessage,
    setMessageEditingMode
} from "../../store/slices/ChatSlice";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        '&:hover': {
            background: COLORS.surface.dark
        },
        display: 'grid',
        gridTemplateColumns: `fit-content(${theme.spacing(6)}px) 1fr`,
        gridGap: theme.spacing(2),
        marginTop: theme.spacing(1),
        padding: theme.spacing(1)
    },
    rootSmall: {
        marginTop: theme.spacing(0),
        padding: theme.spacing(.5, 1, .5, 1)
    },
    avatarContainer: {
        width: theme.spacing(6),
    },
    avatar: {
        height: theme.spacing(6),
        width: theme.spacing(6),
    },
    container: {
        display: 'grid',
        gridGap: theme.spacing(.5)
    },
    containerSmall: {
        display: 'grid',
        gridGap: theme.spacing(.5),
        gridTemplateColumns: 'fit-content(0px) 1fr'
    },
    header: {
        display: 'flex',
        '& > span': {
            marginRight: theme.spacing(1)
        }
    },
    name: {
        fontWeight: 700
    },
    timestamp: {
        color: theme.palette.text.disabled,
    },
    timestampSmall: {
        color: theme.palette.text.disabled,
    },
    message: {},
    menuItem: {
        display: 'flex',
        alignItems: 'center',
        '& > span': {
            paddingLeft: theme.spacing(.5)
        }
    },
    likedByContainer: {
        width: 'fit-content',
        userSelect: 'none',
    }
}))

const initialState = {
    mouseX: null,
    mouseY: null,
};

type LikedByBadgePropsType = {
    likedBy: Array<string>
}

const LikedByBadge: React.FC<LikedByBadgePropsType> = props => {

    const classes = useStyles()

    const {likedBy} = props

    if (!(likedBy.length > 0)) return (
        <></>
    )

    return (
        <Tooltip title={`Понравилось ${likedBy.join(', ')}`} placement={'right'}>
            <IconButton className={classes.likedByContainer}>
                <Badge badgeContent={likedBy.length}
                       color={'primary'}
                       anchorOrigin={{
                           vertical: 'bottom',
                           horizontal: 'right',
                       }}
                >
                    <Typography component={'span'} variant={'h5'} role={'img'} aria-label={'like'}>
                        👍
                    </Typography>
                </Badge>
            </IconButton>
        </Tooltip>
    )
}

type ChatContentItemPropsType = {
    profile: ProfileItemType
    timestamp: string
    message: MessageItemTypeWithState
    likedBy: Array<string>
    size?: 'small' | 'normal'
} & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const ChatContentItem: React.FC<ChatContentItemPropsType> = props => {

    const classes = useStyles()
    const theme = useTheme()

    const {
        profile, timestamp, message,
        size = 'normal', userId, likedBy,
        likeMessage, deleteMessage, setMessageEditingMode,
        editMessage
    } = props

    const userOwnsMessage = profile.id === userId

    const avatarColor = stringToColor(profile.name)
    const avatarStyle = {
        color: theme.palette.getContrastText(avatarColor),
        background: avatarColor
    }

    const [state, setState] = React.useState<{
        mouseX: null | number;
        mouseY: null | number;
    }>(initialState)

    const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
        event.preventDefault();
        setState({
            mouseX: event.clientX - 2,
            mouseY: event.clientY - 4,
        })
    }

    const handleClose = () => {
        setState(initialState);
    }

    const handleDelete = () => {
        deleteMessage(message)
        handleClose()
    }

    const handleLike = () => {
        likeMessage(message)
        handleClose()
    }

    const handleEdit = () => {
        setMessageEditingMode(message)
        handleClose()
    }

    const avatar = (
        <StatusBadge status={profile.status}>
            <Avatar className={classes.avatar}
                    style={{...avatarStyle}}>{profile.name.charAt(0)}
            </Avatar>
        </StatusBadge>
    )

    const nameAndTimestamp = (
        <div className={classes.header}>
            <Typography component={'span'}
                        variant={'subtitle2'}
                        className={classes.name}>
                {profile.name}
            </Typography>
            <Typography component={'span'}
                        variant={'subtitle2'}
                        className={classes.timestamp}>
                {timestamp}
            </Typography>
        </div>
    )

    const smallTimestamp = (
        <Typography component={'span'}
                    variant={'caption'}
                    className={classes.timestampSmall}>
            {timestamp}
        </Typography>
    )

    const userActions = [
        <MenuItem key={'delete'} onClick={handleDelete} className={classes.menuItem}>
            <DeleteIcon/><span>Удалить</span>
        </MenuItem>,
        <MenuItem key={'edit'} onClick={handleEdit} className={classes.menuItem}>
            <EditIcon/><span>Редактировать</span>
        </MenuItem>
    ]

    const messageRef = useRef<HTMLInputElement>(null)

    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if ((event.key === 'Enter') && (messageRef.current)) {
            editMessage(message, messageRef.current.value)
        }
    }

    const messageContainer = (message.editing.editingMode)
        ? <TextField
            fullWidth
            onKeyPress={handleKeyPress}
            defaultValue={message.message}
            inputRef={messageRef}
            variant={'outlined'}
        />
        : <>{message.message}</>

    return (
        <div className={clsx(classes.root, size === 'small' && classes.rootSmall)} onContextMenu={handleClick}>
            <div className={classes.avatarContainer}>
                {(size === 'normal') ? avatar : null}
                {(size === 'small') ? smallTimestamp : null}
            </div>
            <div className={classes.container}>
                {(size === 'normal') ? nameAndTimestamp : null}
                <Typography component={'span'}
                            variant={'body1'}
                            className={classes.message}>
                    {messageContainer}
                </Typography>
                <LikedByBadge likedBy={likedBy}/>
            </div>

            <Menu
                keepMounted
                open={state.mouseY !== null}
                onClose={handleClose}
                anchorReference={'anchorPosition'}
                anchorPosition={
                    state.mouseY !== null && state.mouseX !== null
                        ? {top: state.mouseY, left: state.mouseX}
                        : undefined
                }
            >
                <MenuItem key={'like'} onClick={handleLike}>
                    <Typography component={'span'} variant={'h4'} role={'img'} aria-label={'like'}>
                        👍
                    </Typography>
                </MenuItem>
                {(userOwnsMessage) ? userActions : null}
            </Menu>
        </div>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        userId: state.Session.id,
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        likeMessage: (message: MessageItemTypeWithState) => dispatch(likeMessage(message)),
        deleteMessage: (message: MessageItemTypeWithState) => dispatch(deleteMessageFromDatabase(message)),
        setMessageEditingMode: (message: MessageItemTypeWithState) => {
            dispatch(setMessageEditingMode({message, payload: true}))
        },
        editMessage: (message: MessageItemTypeWithState, payload: string) => {
            dispatch(editMessageInDatabase(message, {message: payload}))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatContentItem)
