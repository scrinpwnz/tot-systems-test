import React, {useEffect} from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {COLORS} from "../../styles/colors";
import ChatTextArea from "./ChatTextArea";
import ChatContent from "./ChatContent";
import ChatHeader from "./ChatHeader";
import Members from "../Members/Members";
import {RouteComponentProps} from "react-router";
import {AppDispatch, RootState} from "../../store/store";
import {connect} from "react-redux";
import {getChatData} from "../../store/slices/ChatSlice";
import {ChatItemWithDataType, MessageItemTypeWithState, ProfileItemType} from "../../types/ChatTypes";
import LoadingPlaceHolder from "../LoadingPlaceHolder";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'grid',
        gridTemplateColumns: `minmax(400px, 1fr) 200px`,
        gridTemplateRows: '100%',
        background: COLORS.surface.main,
    },
    chatContent: {
        display: 'grid',
        gridTemplateRows: `${theme.spacing(6)}px 1fr ${theme.spacing(9)}px`,
        overflow: 'auto'
    }
}))

type ChatTypeProps = RouteComponentProps<any> & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const Chat: React.FC<ChatTypeProps> = props => {

    const classes = useStyles()

    const {match, chats, getChatData} = props
    const chat = chats.find(item => item.id === parseInt(match.params.id))
    let messages = [] as Array<MessageItemTypeWithState>
    let members = [] as Array<ProfileItemType>
    let chatName = '' as string

    if (chat) {
        messages = chat.messages
        members = chat.membersData
        chatName = chat.name
    }

    useEffect(() => {
       if (chat) getChatData(chat)
    }, [match.params.id])

    return (
        <main className={classes.root}>
            <div className={classes.chatContent}>
                <ChatHeader chatName={chatName}/>
                {(members.length > 0 && messages.length > 0) ? <ChatContent messages={messages} members={members}/> : <LoadingPlaceHolder/>}
                <ChatTextArea chat={chat!}/>
            </div>
            <Members members={members}/>
        </main>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        chats: state.Chat.chats,
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        getChatData: (chat: ChatItemWithDataType) => dispatch(getChatData(chat))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
