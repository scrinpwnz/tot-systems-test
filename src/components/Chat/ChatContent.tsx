import React, {useEffect, useRef} from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import ChatContentItem from "./ChatContentItem";
import {MessageItemTypeWithState, ProfileItemType} from "../../types/ChatTypes";
import {getFormattedDate} from "../../global/helpers/helpers";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
        overflow: 'auto',
        "&::-webkit-scrollbar": {
            width: theme.spacing(1),
        },
        "&::-webkit-scrollbar-thumb": {
            background: theme.palette.primary.main
        },
        "&::-webkit-scrollbar-track": {
            background: theme.palette.background.paper
        },
    }
}))

type ChatContentPropsType = {
    messages: Array<MessageItemTypeWithState>
    members: Array<ProfileItemType>
}

const ChatContent: React.FC<ChatContentPropsType> = props => {

    const classes = useStyles()

    const {messages, members} = props

    const chatBottom = useRef<HTMLDivElement>(null)

    const scrollToBottom = () => {
        if (chatBottom.current) chatBottom.current.scrollIntoView()
    }

    useEffect(() => {
        scrollToBottom()
    })

    const items = messages.slice().sort((a, b) => {
        if (a.timestamp < b.timestamp) return -1
        if (a.timestamp > b.timestamp) return 1
        return 0
    }).map((message, index, arr) =>
        <ChatContentItem key={message.id}
                         profile={members.find(member => member.id === message.userId)!}
                         timestamp={getFormattedDate(message.timestamp).time}
                         likedBy={
                             message.likedBy.map(likeId => members.find(member => member.id === likeId)!.name)
                         }
                         size={(arr[index - 1] && arr[index - 1].userId === message.userId) ? 'small' : 'normal'}
                         message={message}/>)

    return (
        <div className={classes.root}>
            {items}
            <div style={{float: "left", clear: "both"}}
                 ref={chatBottom}>
            </div>
        </div>
    )
}

export default ChatContent