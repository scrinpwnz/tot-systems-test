import React from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {COLORS} from "../../styles/colors";
import MembersHeader from "./MembersHeader";
import MembersContent from "./MembersContent";
import {MembersList, ProfileItemType} from "../../types/ChatTypes";
import MembersContentItem from "./MembersContentItem";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'grid',
        gridTemplateRows: `${theme.spacing(6)}px 1fr`,
        background: COLORS.surface.dark,
    }
}))

type MembersPropsType = {
    members: Array<ProfileItemType>
}

const Members: React.FC<MembersPropsType> = props => {

    const classes = useStyles()

    const {members} = props

    const lists = {
        online: [],
        away: [],
        dnd: [],
        offline: []
    } as {
        [key: string]: MembersList
    }

    members.forEach(item => {
        lists[item.status].push(<MembersContentItem key={item.id} status={item.status} name={item.name}/>)
    })

    return (
        <div className={classes.root}>
            <MembersHeader/>
            <MembersContent online={[...lists.online, ...lists.away, ...lists.dnd]} offline={lists.offline}/>
        </div>
    )
}

export default Members