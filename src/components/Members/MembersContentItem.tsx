import React from "react";
import {makeStyles, Theme, useTheme} from "@material-ui/core/styles";
import {stringToColor} from "../../global/helpers/helpers";
import {Avatar, Typography} from "@material-ui/core";
import StatusBadge from "../StatusBadge";
import {UserStatus} from "../../types/UsersTypes";
import {COLORS} from "../../styles/colors";
import clsx from 'clsx'

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'grid',
        gridTemplateColumns: `fit-content(${theme.spacing(4)}px) 1fr`,
        alignItems: 'center',
        gridGap: theme.spacing(1),
        padding: theme.spacing(.5, 2),
    },
    avatar: {
        height: theme.spacing(4),
        width: theme.spacing(4),
    },
    name: {
        color: theme.palette.text.disabled
    },
    hover: {
        cursor: 'pointer',
        '&:hover': {
            background: COLORS.surface.darkest
        },
    }
}))

type MembersContentItemPropsType = {
    status: UserStatus
    name: string
    hover?: boolean
}

const MembersContentItem: React.FC<MembersContentItemPropsType> = props => {

    const classes = useStyles()
    const theme = useTheme()

    const {status, name, hover = true} = props

    const avatarColor = stringToColor(name)
    const avatarStyle = {
        color: theme.palette.getContrastText(avatarColor),
        background: avatarColor
    }

    return (
        <div className={clsx(classes.root, hover && classes.hover)}>
            <StatusBadge status={status}>
                <Avatar className={classes.avatar}
                        style={{...avatarStyle}}>{name.charAt(0).toUpperCase()}
                </Avatar>
            </StatusBadge>
            <Typography component={'span'}
                        variant={'subtitle2'}
                        className={classes.name}>
                {name}
            </Typography>
        </div>
    )
}

export default MembersContentItem