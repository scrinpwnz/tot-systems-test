import React from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {COLORS} from "../../styles/colors";
import MembersContentItem from "./MembersContentItem";
import {RootState} from "../../store/store";
import {connect} from "react-redux";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        borderBottom: `${COLORS.surface.darkest} 1px solid`
    },
    content: {
        display: 'flex',
        userSelect: 'none',
        height: '100%',
        alignItems: 'center',
    }
}))

type MembersHeaderPropsType = ReturnType<typeof mapStateToProps>

const MembersHeader: React.FC<MembersHeaderPropsType> = props => {

    const classes = useStyles()

    let {name} = props

    if (!name) name = ''

    return (
        <div className={classes.root}>
            <div className={classes.content}>
                <MembersContentItem status={'online'} name={name} hover={false}/>
            </div>
        </div>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        name: state.Session.name
    }
}

export default connect(mapStateToProps)(MembersHeader)