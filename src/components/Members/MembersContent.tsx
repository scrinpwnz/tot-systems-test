import React from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {Typography} from "@material-ui/core";
import {MembersList} from "../../types/ChatTypes";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        overflow: 'auto',
        "&::-webkit-scrollbar": {
            width: theme.spacing(1),
        },
        "&::-webkit-scrollbar-thumb": {
            background: theme.palette.primary.main
        },
        "&::-webkit-scrollbar-track": {
            background: theme.palette.background.paper
        },
        paddingBottom: theme.spacing(2)
    },
    title: {
        padding: theme.spacing(1, 0, 1, 2)
    }
}))

type MembersContentStatusList = {
    list: MembersList
    status: 'online' | 'offline'
}

const MembersContentStatusList: React.FC<MembersContentStatusList> = props => {

    const classes = useStyles()

    const {list, status} = props

    return (
        <>
            <div className={classes.title}>
                <Typography variant={'body1'} component={'span'}>
                    {(status === 'offline') ? 'Не в сети — ' : 'В сети — '}{list.length}
                </Typography>
            </div>
            {list}
        </>
    )
}

type MembersContentPropsType = {
    online: MembersList
    offline?: MembersList
}

const MembersContent: React.FC<MembersContentPropsType> = props => {

    const classes = useStyles()

    const {online, offline = []} = props

    return (
        <div className={classes.root}>
            <MembersContentStatusList list={online} status={'online'}/>
            {(offline.length > 0) ? <MembersContentStatusList list={offline} status={'offline'}/> : null}
        </div>
    )
}

export default MembersContent