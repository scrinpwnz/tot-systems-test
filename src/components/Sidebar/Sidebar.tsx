import React from 'react'
import {connect} from "react-redux"
import {AppDispatch, RootState} from "../../store/store"
import {toggle} from "../../store/slices/SidebarSlice"
import clsx from "clsx";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Drawer from "@material-ui/core/Drawer";
import {makeStyles, Theme} from "@material-ui/core/styles";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import CheckIcon from '@material-ui/icons/Check';
import {logout} from "../../store/slices/LoginSlice";
import {CircularProgress} from "@material-ui/core";
import {SIZES} from "../../styles/sizes";
import ChatTabs from "./ChatTabs";

const useStyles = makeStyles((theme: Theme) => ({
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(1, .5)
    },
    drawerPaper: {
        position: 'relative',
        width: SIZES.sidebarOpened,
        padding: 0,
        background: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.easeInOut,
            duration: theme.transitions.duration.standard,
        }),
        overflow: 'hidden'
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        width: theme.spacing(8)
    },
    logout: {
        display: 'flex',
        flex: '1',
        flexDirection: 'column-reverse',
        alignItems: 'center',
        paddingBottom: theme.spacing(4),
    },
    iconButton: {
        color: 'inherit'
    },
    fabLoading: {
        position: 'absolute',
        left: '0',
        top: '0',
        zIndex: 1,
    },
}))

type Props = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const Sidebar: React.FC<Props> = props => {

    const classes = useStyles()

    const {open, chats, toggle, logout, token, logoutInProgress} = props

    return (
        <Drawer
            variant={"permanent"}
            classes={{
                paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
            }}
            open={open}
        >
            <div className={classes.toolbarIcon}>
                <IconButton onClick={toggle}
                            classes={{root: classes.iconButton}}>
                    {open ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                </IconButton>
            </div>
            <ChatTabs open={open} tabs={chats}/>
            <div className={classes.logout}>
                <div style={{position: 'relative'}}>
                    <IconButton onClick={logout}
                                classes={{root: classes.iconButton}}>
                        {(token) ? <ExitToAppIcon/> : <CheckIcon/>}
                    </IconButton>
                    {logoutInProgress &&
                    <CircularProgress size={48} className={classes.fabLoading} color={'secondary'}/>}
                </div>
            </div>
        </Drawer>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        chats: state.Chat.chats,
        open: state.Sidebar.open,
        token: state.Session.token,
        logoutInProgress: state.Login.logoutInProgress
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        toggle: () => dispatch(toggle()),
        logout: () => dispatch(logout()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)














