import React from "react";
import clsx from "clsx";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import {RouteComponentProps, withRouter} from "react-router";
import {ChatItemType} from "../../types/ChatTypes";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {SIZES} from "../../styles/sizes";
import DashboardIcon from '@material-ui/icons/Dashboard';
import ContactsIcon from '@material-ui/icons/Contacts';

const useStyles = makeStyles((theme: Theme) => ({
    tabsIndicator: {
        width: theme.spacing(.5)
    },
    tabRoot: {
        whiteSpace: 'nowrap',
        minWidth: SIZES.sidebarOpened
    },
    tabWrapperClose: {
        flexDirection: 'row',
        justifyContent: 'left',
    },
    tabIconClose: {
        '& :first-child': {
            minWidth: theme.spacing(5),
            marginRight: theme.spacing(2),
            marginBottom: 0
        }
    }
}))

type IconsType = {
    [key: string]: React.ReactElement
}

const icons: IconsType = {
    dashboard: <DashboardIcon/>,
    contacts: <ContactsIcon/>,
}

type TabsPropsType = {
    tabs: Array<ChatItemType>
    open: boolean
} & RouteComponentProps<any>

const ChatTabs: React.FC<TabsPropsType> = props => {

    const classes = useStyles()

    const {tabs, open} = props

    const handleCallToRouter = (e: React.ChangeEvent<{}>, value: string) => {
        props.history.push(value)
    }

    const tabClasses = {
        classes: {
            wrapper: clsx(!open && classes.tabWrapperClose),
            labelIcon: clsx(!open && classes.tabIconClose),
            root: classes.tabRoot
        }
    }

    const result = tabs.map(tab =>
        <Tab key={tab.id} icon={icons[tab.icon]} label={tab.name} value={`/chat/${tab.id}`} {...tabClasses}/>
    )

    return (
        <Tabs
            orientation={"vertical"}
            value={props.history.location.pathname}
            onChange={handleCallToRouter}
            classes={{
                indicator: classes.tabsIndicator,
            }}
        >
            {result}>
        </Tabs>
    )
}

export default withRouter(ChatTabs)