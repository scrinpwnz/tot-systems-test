import React from "react";
import {Badge, createStyles, Theme, withStyles} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {COLORS} from "../styles/colors";
import clsx from 'clsx'
import {UserStatus} from "../types/UsersTypes";


const StyledBadge = withStyles((theme: Theme) =>
    createStyles({
        badge: {
            backgroundColor: '#44b700',
            color: '#44b700',
            boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        },
    }),
)(Badge);

const useStyles = makeStyles((theme: Theme) => ({
    badge: {
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    },
    online: {
        backgroundColor: COLORS.status.online,
        color: COLORS.status.online,
    },
    offline: {
        backgroundColor: COLORS.status.offline,
        color: COLORS.status.offline,
    },
    away: {
        backgroundColor: COLORS.status.away,
        color: COLORS.status.away,
    },
    dnd: {
        backgroundColor: COLORS.status.dnd,
        color: COLORS.status.dnd,
    }
}))

type StatusBadgeTypeProps = {
    status: UserStatus
}

const StatusBadge: React.FC<StatusBadgeTypeProps> = props => {

    const classes = useStyles()

    const {status} = props

    return (
        <StyledBadge overlap={'circle'}
                     anchorOrigin={{
                         vertical: 'bottom',
                         horizontal: 'right',
                     }}
                     classes={{
                         badge: clsx(classes.badge, classes[status])
                     }}
                     variant={'dot'}>
            {props.children}
        </StyledBadge>
    )
}

export default StatusBadge