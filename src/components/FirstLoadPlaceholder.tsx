import React from 'react'
import {makeStyles, Theme} from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'

const useStyles = makeStyles((theme: Theme) => ({
    container: {
        display: 'grid',
        placeItems: 'center',
        height: '100vh'
    },
}))

const FirstLoadPlaceholder = () => {

    const classes = useStyles()

    return (
        <div className={classes.container}>
            <CircularProgress size={200} variant="indeterminate"/>
        </div>
    )
}


export default FirstLoadPlaceholder